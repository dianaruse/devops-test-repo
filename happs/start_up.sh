#!/bin/sh

cd /var/www/project
cp -r /var/www/composer-cache/* ./

composer dump-autoload --optimize

php artisan migrate --seed
php artisan serve --host=0.0.0.0