# Containerization of Laravel application with php-fpm and mariadb in Docker

This project is a docker compose installation of a Laravel application instance using php-fpm and mariadb as the database. Docker files are used for creating the images for each service.

### Requirements

Happs application can be deployed easily on Windows or Linux machines through docker. Install Docker as described in the links, depending on the environment that you work on: [Windows](https://docs.docker.com/docker-for-windows/install/) or [Linux](https://docs.docker.com/install/linux/docker-ce/ubuntu/).

### Prerequisites

Replace the database connection from ***haaps/.env*** file with your credential database, in this section:

```DB_HOST=<your_db_service_name>```
```DB_DATABASE=<your_db_name>```
```DB_USERNAME=<your_db_user_name>```
```DB_PASSWORD=<your_db_pass>```

For this example, my *.env* file looks like this:

```
DB_CONNECTION=mysql
DB_HOST=database
DB_PORT=3306
DB_DATABASE=happs
DB_USERNAME=admin
DB_PASSWORD=admin
```
Normally, in a production environment, this file should be added to .gitignore and those values should not be exposed to public. This is a test example.

### Describing the steps

For docker-compose I've build two images that can be found on *haaps/Dockerfile* for php service and *mysql/Dockerfile* for mariadb service.

The decisions taken in order to create php image are:

- All database work in Laravel is done through the PHP PDO facilities so I had to add the driver for mysql database, which is compatible with mariadb too.
- I created a cache folder in container so that the composer packages already installed won't be recreated in a build. I also copied the composer.lock file that was generated with *composer install* command in happs application.
- It is recommanded that the  installation of composer to be done with other user than root, so I created *phpuser* for this.
- The *start_up.sh* script initialize the application.

The docker compose has *./happs* mounted on application home directory created in php Dockerfile */var/www/project*, which help developers to easily change the code deployed on their local docker container without rebuilding the image.

### Running the application

- Clone the repository on your local machine.
- Run ```docker-compose up -d --build```.

The application can be accessed from your browser on ```http://localhost:8080```.
